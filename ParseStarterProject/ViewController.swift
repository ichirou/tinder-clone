/**
* Copyright (c) 2015-present, Parse, LLC.
* All rights reserved.
*
* This source code is licensed under the BSD-style license found in the
* LICENSE file in the root directory of this source tree. An additional grant
* of patent rights can be found in the PATENTS file in the same directory.
*/

import UIKit
import Parse

class ViewController: UIViewController {

    var signupMode = true
    
    @IBOutlet var changeSignupModeButton: UIButton!
    @IBOutlet var signupOrLoginButton: UIButton!
    @IBOutlet var usernameField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var errorLabel: UILabel!
    
    func redirectUser() {
        if PFUser.current() != nil {
            
            if PFUser.current()?["isFemale"] != nil && PFUser.current()?["isInterestedInWomen"] != nil && PFUser.current()?["photo"] as? PFFile != nil {
                
                performSegue(withIdentifier: "swipeFromInitialSegue", sender: self)
                
            } else {
                performSegue(withIdentifier: "goToUserInfo", sender: self)
            }
        }
    }
    
    @IBAction func signupOrLogin(_ sender: AnyObject) {
        self.errorLabel.text = ""
        
        if signupMode {
            let user = PFUser()
            
            if usernameField.text == "" {
                self.errorLabel.text = "Username is required!"
            } else {
                let acl = PFACL()
                acl.getPublicWriteAccess = true
                acl.getPublicReadAccess = true
                
                user.acl = acl
                user.username = usernameField.text
                user.password = passwordField.text
                
                user.signUpInBackground { (success, error) in
                    
                    if error != nil {
                        var errorMessage = "Signup failed - please try again"
                        
                        let error = error as NSError?
                        
                        if let parseError = error?.userInfo["error"] as? String {
                            errorMessage = parseError
                        }
                        
                        self.errorLabel.text = errorMessage
                    } else {
                        
                        print("Signed up")
                        self.performSegue(withIdentifier: "goToUserInfo", sender: self)

                    }
                }
            }
        }//end signupmode
        else {// login mode
            PFUser.logInWithUsername(inBackground: usernameField.text!, password: passwordField.text!, block: { (user, error) in
                
                if error != nil {
                    var errorMessage = "Login failed - please try again"
                    
                    let error = error as NSError?
                    
                    if let parseError = error?.userInfo["error"] as? String {
                        errorMessage = parseError
                    }
                    
                    self.errorLabel.text = errorMessage
                } else {
                    
                    print("Logged In")
                    self.redirectUser()

                }

            })//end login background
        }
        
    }
    
    @IBAction func changeSignupMode(_ sender: AnyObject) {
        
        if signupMode {
            signupMode = false
            signupOrLoginButton.setTitle("Login", for: [])
            changeSignupModeButton.setTitle("Sign Up", for: [])
        } else {
            signupMode = true
            signupOrLoginButton.setTitle("Sign Up", for: [])
            changeSignupModeButton.setTitle("Login", for: [])

        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.redirectUser()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
