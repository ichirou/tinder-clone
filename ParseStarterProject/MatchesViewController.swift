//
//  MatchesViewController.swift
//  TinderClone
//
//  Created by Richmond Ko on 21/09/2016.
//  Copyright © 2016 Parse. All rights reserved.
//

import UIKit
import Parse

class MatchesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var images = [UIImage]()
    var userIds = [String]()
    var messages = [String] ()
    
    @IBOutlet var tableView: UITableView!
    
    var activityIndicator = UIActivityIndicatorView()
    
    func showLoading() {
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    func stopLoading() {
        self.activityIndicator.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        showLoading()
        let query = PFUser.query()
        //get matched users
        query?.whereKey("accepted", contains: PFUser.current()?.objectId)
        query?.whereKey("objectId", containedIn: PFUser.current()?["accepted"] as! [String])
        query?.findObjectsInBackground(block: { (objects, error) in
            if let users = objects {
                for object in users {
                    if let user = object as? PFUser {
                        let imageFile = user["photo"] as! PFFile
                        //get image of matched user
                        imageFile.getDataInBackground(block: { (data, error) in
                            if let imageData = data {
                                
                                //get message of matched user
                                let messageQuery = PFQuery(className: "Message")
                                messageQuery.whereKey("recipient", equalTo: (PFUser.current()?.objectId!)!)
                                messageQuery.whereKey("sender", equalTo: user.objectId!)
                                
                                messageQuery.findObjectsInBackground(block: { (objects, error) in
                                    var messageText = "No message from this user."
                                    if let objects = objects {
                                        for message in objects {
                                            if let messageContent = message["content"] as? String {
                                                messageText = messageContent
                                            }
                                        }
                                    }
                                    
                                    //append everything
                                    self.images.append(UIImage(data: imageData)!)
                                    self.userIds.append(user.objectId!)
                                    self.messages.append(messageText)
                                    self.tableView.reloadData()
                                })//end get messages of matches
                            }
                        })//end get images of matches
                    }
                }
            }
            self.stopLoading()
        })//end find matches
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return images.count
    }

    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MatchesTableViewCell
        
        cell.matchImageView.image = images[indexPath.row]
        cell.messageLabel.text = messages[indexPath.row]
        cell.userIdLabel.text = userIds[indexPath.row]
        return cell
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
