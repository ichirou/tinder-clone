//
//  MatchesTableViewCell.swift
//  TinderClone
//
//  Created by Richmond Ko on 21/09/2016.
//  Copyright © 2016 Parse. All rights reserved.
//

import UIKit
import Parse

class MatchesTableViewCell: UITableViewCell {

    @IBOutlet var userNotif: UILabel!
    @IBOutlet var userIdLabel: UILabel!
    @IBOutlet var messageText: UITextField!
    @IBOutlet var messageLabel: UILabel!
    @IBOutlet var matchImageView: UIImageView!
    
    var activityIndicator = UIActivityIndicatorView()
    
    func showLoading() {
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        activityIndicator.center = self.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        self.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    func stopLoading() {
        self.activityIndicator.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    @IBAction func send(_ sender: AnyObject) {
        showLoading()
        print(userIdLabel.text)
        print(messageText.text)
        
        let message = PFObject(className: "Message")
        message["sender"] = PFUser.current()?.objectId
        message["recipient"] = userIdLabel.text
        message["content"] = messageText.text
        
        message.saveInBackground { (success, error) in
            if success {
                self.userNotif.text = "Message sent!"
            } else if error != nil {
    
                let error = error as NSError?
                
                if let errorMessage = error?.userInfo["error"] as? String {
                    self.userNotif.text = errorMessage
                }
            } else {
                self.userNotif.text = "Message sending failed! Try again later."
            }
            self.stopLoading()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
