//
//  SwipingViewController.swift
//  TinderClone
//
//  Created by Richmond Ko on 20/09/2016.
//  Copyright © 2016 Parse. All rights reserved.
//

import UIKit
import Parse

class SwipingViewController: UIViewController {

    @IBOutlet var errorLabel: UILabel!
    @IBOutlet var imageView: UIImageView!
    var displayedUserID = ""
    var activityIndicator = UIActivityIndicatorView()
    
    func showLoading() {
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    func stopLoading() {
        self.activityIndicator.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    func wasDragged(gestureRecognizer: UIPanGestureRecognizer) {
        let translation = gestureRecognizer.translation(in: view)
        let label = gestureRecognizer.view!
        
        label.center = CGPoint(x: self.view.bounds.width / 2 + translation.x, y: self.view.bounds.height / 2 + translation.y)
        
        let xFromCenter = label.center.x - self.view.bounds.width / 2
        
        var rotation = CGAffineTransform(rotationAngle: xFromCenter / 200)
        let scale = min(abs(100 / xFromCenter), 1)
        var stretchAndRotation = rotation.scaledBy(x: scale, y: scale)
        
        label.transform = stretchAndRotation
        
        if gestureRecognizer.state == UIGestureRecognizerState.ended {
            
            var acceptedOrRejected = ""
            
            if label.center.x < 100 {
                acceptedOrRejected = "rejected"
            }
            else if label.center.x > self.view.bounds.width - 100 {
                acceptedOrRejected = "accepted"
            }
            
            if acceptedOrRejected != "" && displayedUserID != "" {
                PFUser.current()?.addUniqueObjects(from: [displayedUserID], forKey: acceptedOrRejected)
                
                PFUser.current()?.saveInBackground(block: { (success, error) in
                    if error != nil {
                        let error = error as NSError?
                        
                        if let parseError = error?.userInfo["error"] as? String {
                            self.errorLabel.text = parseError
                        }
                    } else {
                        //print(PFUser.current())
                        self.updateImage()
                    }
                })
            }
            
            rotation = CGAffineTransform(rotationAngle: 0)
            stretchAndRotation = rotation.scaledBy(x: 1, y: 1)
            label.transform = stretchAndRotation
            label.center = CGPoint(x: self.view.bounds.width / 2, y: self.view.bounds.height / 2)
        }
    }

    func updateImage() {
        showLoading()
        let query = PFUser.query()
        
        var ignoredUsers = [""]
        
        if let acceptedUsers = PFUser.current()?["accepted"] {
            ignoredUsers += acceptedUsers as! Array
        }
        
        if let rejectedUsers = PFUser.current()?["rejected"] {
            ignoredUsers += rejectedUsers as! Array
        }
        
        query?.whereKey("isFemale", equalTo: (PFUser.current()?["isInterestedInWomen"])!)
        query?.whereKey("isInterestedInWomen", equalTo: (PFUser.current()?["isFemale"])!)
        query?.whereKey("objectId", notContainedIn: ignoredUsers)
        
        if let location = PFUser.current()?["location"] as? PFGeoPoint {
            let latitude = location.latitude
            let longtitude = location.longitude
            query?.whereKey("location", withinGeoBoxFromSouthwest: PFGeoPoint(latitude: latitude - 1, longitude: longtitude - 1), toNortheast: PFGeoPoint(latitude: latitude + 1, longitude: longtitude + 1))
        }
        
        query?.limit = 1
        
        query?.findObjectsInBackground(block: { (objects, error) in
            if error != nil {
                let error = error as NSError?
                
                if let parseError = error?.userInfo["error"] as? String {
                    self.errorLabel.text = parseError
                }
                self.stopLoading()
            }
            else if let users = objects {
                print("AAAAAAAAA: found some objects!")
                print(users)
                if users.count != 0 {
                    for object in users {
                        if let user = object as? PFUser {
                            
                            self.displayedUserID = user.objectId!
                            
                            let imageFile = user["photo"] as! PFFile
                            
                            imageFile.getDataInBackground(block: { (data, error) in
                                if let imageData = data {
                                    self.imageView.image = UIImage(data: imageData)
                                }
                            })
                        }
                    }
                    self.stopLoading()
                }
                else {
                    self.imageView.image = UIImage(named: "person-icon-5.png")
                    self.errorLabel.text = "No more users available to swipe, try again later"
                    print("NORE MORE USERSRERSDSRSR")
                    self.stopLoading()
                }
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateImage()
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(self.wasDragged(gestureRecognizer: )))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(gesture)
        
        PFGeoPoint.geoPointForCurrentLocation { (geopoint, error) in
            if let geopoint = geopoint {
                PFUser.current()?["location"] = geopoint
                
                PFUser.current()?.saveInBackground()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "logoutSegue" {
            PFUser.logOut()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
