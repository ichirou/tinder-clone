//
//  UserDetailsViewController.swift
//  TinderClone
//
//  Created by Richmond Ko on 20/09/2016.
//  Copyright © 2016 Parse. All rights reserved.
//

import UIKit
import Parse

class UserDetailsViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    var activityIndicator = UIActivityIndicatorView()
    
    @IBAction func logout(_ sender: AnyObject) {
        
        PFUser.logOut()
    }
    
    @IBOutlet var errorLabel: UILabel!
    @IBOutlet var userImage: UIImageView!
    @IBOutlet var genderSwitch: UISwitch!
    @IBOutlet var interestedInSwitch: UISwitch!
    
    @IBAction func updateProfileImage(_ sender: AnyObject) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func showLoading() {
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    func stopLoading() {
        self.activityIndicator.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            userImage.image = image
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func update(_ sender: AnyObject) {
        showLoading()
        PFUser.current()?["isFemale"] = genderSwitch.isOn
        PFUser.current()?["isInterestedInWomen"] = interestedInSwitch.isOn
        
        let imageData = UIImageJPEGRepresentation(userImage.image!, 0.5)
        PFUser.current()?["photo"] = PFFile(name: "profile.jpg", data: imageData!)
        
        PFUser.current()?.saveInBackground(block: { (success, error) in
            if error != nil {
                var errorMessage = "Update failed - please try again"
                
                let error = error as NSError?
                
                if let parseError = error?.userInfo["error"] as? String {
                    errorMessage = parseError
                }
                
                self.errorLabel.text = errorMessage
                self.stopLoading()
            } else {
                
                print("Updated")
                self.performSegue(withIdentifier: "showSwipingViewController", sender: self)
                self.stopLoading()
            }

        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        showLoading()
        if let isFemale = PFUser.current()?["isFemale"] as? Bool {
            genderSwitch.setOn(isFemale, animated: false)
        }
        
        if let isInterestedInWomen = PFUser.current()?["isInterestedInWomen"] as? Bool {
            interestedInSwitch.setOn(isInterestedInWomen, animated: false)
        }
        
        if let photo = PFUser.current()?["photo"] as? PFFile {
            photo.getDataInBackground(block: { (data, error) in
                if let imageData = data {
                    if let downloadedImage = UIImage(data: imageData) {
                        self.userImage.image = downloadedImage
                    }
                }
            })
        }
        stopLoading()
        /*//script to add users
        let urlArray = ["http://s.orzzzz.com/news/00/72//564431e9b121b.jpg", "https://s-media-cache-ak0.pinimg.com/236x/9a/f1/6a/9af16a2c96ffbe92a2bff0e451745f48.jpg", "http://www.telegraph.co.uk/content/dam/films/2016/04/12/DC_Daria1_2752161k-large_trans++qVzuuqpFlyLIwiB6NTmJwfSVWeZ_vEN7c6bHu2jJnT8.jpg", "http://s.orzzzz.com/news/7c/2a//564431e9b13ed.jpg", "https://itfinspiringwomen.files.wordpress.com/2014/03/scooby-doo-tv-09.jpg", "http://content8.flixster.com/question/62/70/45/6270450_std.jpg"]
        
        var counter = 0
        
        for urlString in urlArray {
            counter += 1
            let url = URL(string: urlString)!
            
            do {
                let data = try Data(contentsOf: url)
                let imageFile = PFFile(name: "photo.png", data: data)
                let user = PFUser()
                user["photo"] = imageFile
                user.username = "female" + String(counter)
                user.password = "password"
                user["isInterestedInWomen"] = false
                user["isFemale"] = true
                let acl = PFACL()
                acl.getPublicWriteAccess = true
                user.acl = acl
                
                user.signUpInBackground(block: { (success, error) in
                    if success {
                        print("user signed up")
                    }
                })
                
            } catch {
                print("Could not get data")
            }
        }
         */
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
